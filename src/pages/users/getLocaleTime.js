function addZero(t){
    return String(t).padStart(2, '0');
}
function getLocaleTime(){
    let YY = addZero(new Date().getFullYear())+'-';
	let MM = addZero(new Date().getMonth()+1)+'-';
	let DD = addZero(new Date().getDate())+' ';
	let hh = addZero(new Date().getHours())+':';
	let mm = addZero(new Date().getMinutes())+':';
	let ss = addZero(new Date().getSeconds());
	let nowTime = YY+MM+DD+hh+mm+ss;
	    
	return nowTime;
}

export default getLocaleTime