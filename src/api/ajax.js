import axios from 'axios'
import qs from 'qs'

/**
 * post 
 * @param url
 * @param data
 * @returns {Promise}
 * qs.stringify()将对象 序列化成URL的形式，以&进行拼接
 */
export function post (url, data = {}) {
    return new Promise((resolve, reject) => {
    	axios.post(url, qs.stringify(data))
            .then(res => {
                resolve(res.data);
            }, err => {
                reject(err);
            })
    });
}

/**
 * get 
 * @param url
 * @param data
 * @returns {Promise}
 * `params` 是即将与请求一起发送的 URL 参数
 */
export function get (url, data = {}) {
    return new Promise((resolve, reject) => {
    	axios.get(url, {params: data})
    	    .then(res => {
                resolve(res.data);
            }, err => {
                reject(err);
            })
    });
}

/**
 * delect
 * @param url
 * @param data
 * @returns {Promise}
 */
export function del (url, data = {}){
	return new Promise((resolve, reject) =>{
		axios.delete(url,{params:data})
		     .then(res =>{
		     	resolve(res.data);
		     }, err =>{
		     	reject(err);
		     })
	})
}
export default axios; 