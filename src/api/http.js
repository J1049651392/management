function http_get(path, arg, callback, type) {
    http_ajax(path, arg, callback, type, 'GET');
}
function http_post(path, arg, callback, type) {
    http_ajax(path, arg, callback, type, 'POST');
}
function http_ajax(path, arg, callback, type, method){
    if(arg == undefined){ arg = {}}
    if(type == undefined){ type = 'json' }
    if(api_token && api_token != ''){ arg.token = api_token }

    //补充签名随机数、数据签名
    var nonce = genSignNonce();
    var sign = getDataSign(Object.assign({}, arg, { nonce: nonce }));

    //追加链接
    var url = api_url + path;
    var append = 'nonce=' + nonce + '&sign=' + sign;
    if (url.indexOf('?') > -1) {
        url = url + '&' + append;
    } else {
        url = url + '?' + append;
    }

    //发起请求
    $.ajax({
        url: url,
        type: method,
        cache: true,
        data: arg,
        dataType: 'text/html',
        complete: function (xhr, ts) {
            if(ts == 'error'){
                if(app.notify){ app.notify(res) }
                else{ alert(res) }
                return;
            }
            var res = xhr.responseText;
            if(type == 'json'){
                try{
                    res = JSON.parse(res);
                }catch (e) {
                    if(app.notify){ app.notify(e); setTimeout(function () {
                        app.notify(res);
                    }, 50); }
                    else{ alert(e);alert(res)  }
                    return;
                }
            }
            if(res.status != undefined && res.status != 200){
                app.loading = false;
                if(res.status === 2011) { logOut() }
                if(app.notify){ app.notify(res.msg) }
                else{ alert(res.msg) }
                return;
            }
            if(callback) {
               if(typeof(callback) === 'function'){
                   callback(res.data);
               } else {
                   for (var i in callback) {
                      callback[i](res.data);
                   }
               }
            }
            // 释放内存 （避免页面不刷新，长时间请求造成内存溢出）
            xhr = null;
        }
    });
}