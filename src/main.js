// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import IView from 'iview'
import axios from './api/ajax'
import 'iview/dist/styles/iview.css'    

Vue.config.productionTip = false
Vue.use(IView)
Vue.prototype.$ajax = axios   //在vue的原型上添加该实例方法$ajax

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  axios,
  components: { App },
  template: '<App/>'
})
