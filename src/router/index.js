import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/pages/login/Login.vue'
import addUser from '@/pages/users/addUser.vue'
import zhuye from '@/pages/Main.vue'
import userinquire from '@/pages/users/UserInquire'

Vue.use(Router)

export default new Router({
	mode: "history",
	routes: [{
			path: '/login',
			name: 'Login',
			component: Login
	    },
		{
			path: '/zhuye',
			name: "zhuye",
			component: zhuye,
			children: [
					{path: '/zhuye/userinquire',name: 'userinquire',component: userinquire},
					{path: '/zhuye/add',name: "addUser",component: addUser},
			]
		},
		{
			path: "*",
			redirect: "Login"
		}

	]
})